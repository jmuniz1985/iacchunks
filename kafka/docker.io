#start-kafka.sh should run after install, we'll see

containerTemplate(
    name: 'kafka',
    image: 'quay.io/jamftest/now-kafka-all-in-one:1.1.0.B',
    resourceRequestMemory: '500Mi',
    ttyEnabled: true,
    ports: [
        portMapping(name: 'zookeeper', containerPort: 2181, hostPort: 2181),
        portMapping(name: 'kafka', containerPort: 9092, hostPort: 9092)
    ],
    command: 'supervisord -n',
    envVars: [
        containerEnvVar(key: 'ADVERTISED_HOST', value: 'localhost')
    ]
),
